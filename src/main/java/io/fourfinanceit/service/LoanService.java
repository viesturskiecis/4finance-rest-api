package io.fourfinanceit.service;

import io.fourfinanceit.data.Loan;
import io.fourfinanceit.data.LoanExtension;
import io.fourfinanceit.data.dto.LoanExtensionRequestDto;
import io.fourfinanceit.data.dto.LoanRequestDto;
import io.fourfinanceit.data.dto.LoanResultDto;
import io.fourfinanceit.repository.LoanExtensionRepository;
import io.fourfinanceit.repository.LoanRepository;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.ParseException;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static io.fourfinanceit.helper.Constants.INTEREST_RATE_PER_WEEK;
import static io.fourfinanceit.helper.DateUtil.addDays;
import static io.fourfinanceit.helper.DateUtil.getWeeksBetween;

@Slf4j
@Service
public class LoanService {
    @Autowired
    private LoanRepository loanRepository;

    @Autowired
    private LoanExtensionRepository loanExtensionRepository;

    @Autowired
    private ModelMapper modelMapper;

    public List<LoanResultDto> getLoans() {
        List<Loan> loanList = loanRepository.findAll();
        return loanList.stream()
                .map(loan -> convertToDto(loan))
                .collect(Collectors.toList());
    }

    public Loan addLoan(LoanRequestDto loanRequestDto) {
        Loan loan = convertToEntity(loanRequestDto);
        return loanRepository.save(loan);
    }

    public LoanExtension addLoanExtension(Long loanId, LoanExtensionRequestDto loanExtensionRequestDto) {
        if (!loanRepository.exists(loanId)) {
            return null;
        }
        LoanExtension loanExtension = modelMapper.map(loanExtensionRequestDto, LoanExtension.class);
        loanExtension.setLoanId(loanId);
        return loanExtensionRepository.save(loanExtension);
    }

    public LoanResultDto convertToDto(Loan loan) {
        LoanResultDto loanResultDto = modelMapper.map(loan, LoanResultDto.class);
        if (!loan.getLoanExtensions().isEmpty()) {
            int extensionCount = loan.getLoanExtensions().size();
            Integer extensionTermDays = loan.getLoanExtensions().stream().mapToInt(LoanExtension::getExtension).sum();
            loanResultDto.setExtensionTerm(extensionTermDays);
            loanResultDto.setExtensionCount(extensionCount);
            loanResultDto.setExpiresWithExtensions(addDays(loan.getExpires(), extensionTermDays));
            loanResultDto.setAmountToPay(calculateAmountWithInterest(loan.getAmount(), loan.getExpires(), loanResultDto.getExpiresWithExtensions()));
        } else {
            loanResultDto.setAmountToPay(loan.getAmount());
        }

        return loanResultDto;
    }

    private Loan convertToEntity(LoanRequestDto loanRequestDto) throws ParseException {
        Loan loan = modelMapper.map(loanRequestDto, Loan.class);
        loan.setExpires(addDays(loanRequestDto.getCreated(), loanRequestDto.getTerm()));
        return loan;
    }

    private double calculateAmountWithInterest(double amountTaken, Date loanExpires, Date loanExpiresWithExtensions) {
        int extensionWeeks = getWeeksBetween(loanExpires, loanExpiresWithExtensions);
        amountTaken += amountTaken * extensionWeeks * INTEREST_RATE_PER_WEEK;
        return amountTaken;
    }
}
