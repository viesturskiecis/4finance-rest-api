package io.fourfinanceit.service;

import io.fourfinanceit.data.Transaction;
import io.fourfinanceit.repository.TransactionRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

@Slf4j
@Component
public class TransactionService {

    @Autowired
    private TransactionRepository transactionRepository;

    public Transaction saveTransaction(String ipAddress) {
        Transaction transactionToSave = new Transaction();
        transactionToSave.setIpAddress(ipAddress);
        return transactionRepository.save(transactionToSave);
    }

    public Long getTransactionCountForDate(String ipAddress, Date date) {
        Long transactionCount = transactionRepository.countByIpAddressAndCreated(ipAddress, date);
        return transactionCount;
    }
}
