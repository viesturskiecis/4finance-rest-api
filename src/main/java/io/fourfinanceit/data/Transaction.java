package io.fourfinanceit.data;

import lombok.Data;
import lombok.NonNull;
import org.joda.time.DateTime;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "transaction")
@Data
public class Transaction {
    @Id
    @GeneratedValue
    private Long id;

    @Column
    @Temporal(TemporalType.DATE)
    private Date created = DateTime.now().toDate();

    @Column
    private String ipAddress;
}
