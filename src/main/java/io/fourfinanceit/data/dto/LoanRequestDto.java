package io.fourfinanceit.data.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.joda.time.DateTime;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.util.Date;

import static io.fourfinanceit.helper.Constants.*;

@Data
@NoArgsConstructor
public class LoanRequestDto {
    @Max(MAX_AMOUNT)
    @Min(MIN_AMOUNT)
    private double amount;

    @Max(MAX_TERM)
    @Min(MIN_TERM)
    private int term;

    @JsonIgnore
    private Date created = DateTime.now().toDate();

    public LoanRequestDto(double amount, int term) {
        this.amount = amount;
        this.term = term;
    }

    public LoanRequestDto(double amount, int term, Date created) {
        this.amount = amount;
        this.term = term;
        this.created = created;
    }
}
