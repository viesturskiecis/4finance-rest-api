package io.fourfinanceit.data.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
@NoArgsConstructor
public class LoanResultDto {
    private int term;

    private int extensionTerm;

    private double amount;

    private double amountToPay;

    private int extensionCount;

    private Date created;

    private Date expires;

    private Date expiresWithExtensions;
}
