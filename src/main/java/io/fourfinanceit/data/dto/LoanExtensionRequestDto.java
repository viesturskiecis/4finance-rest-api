package io.fourfinanceit.data.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.joda.time.DateTime;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.util.Date;

import static io.fourfinanceit.helper.Constants.MAX_TERM;
import static io.fourfinanceit.helper.Constants.MIN_TERM;

@Data
@NoArgsConstructor
public class LoanExtensionRequestDto {
    @Max(MAX_TERM)
    @Min(MIN_TERM)
    private int extension;

    @JsonIgnore
    private Date created = DateTime.now().toDate();

    public LoanExtensionRequestDto(int extension) {
        this.extension = extension;
    }
}
