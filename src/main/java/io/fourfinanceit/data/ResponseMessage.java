package io.fourfinanceit.data;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ResponseMessage {
    private String code;
    private String message;

    public ResponseMessage(String code, String message) {
        this.code = code;
        this.message = message;
    }
}
