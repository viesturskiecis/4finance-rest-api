package io.fourfinanceit.data;

import lombok.Data;
import lombok.NonNull;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "loan_extension")
@Data
public class LoanExtension {
    @Id
    @GeneratedValue
    private Long id;

    @Column
    private int extension;

    @Column
    private Date created;

    @Column(name = "loan_id")
    private Long loanId;
}
