package io.fourfinanceit.data;

import lombok.Data;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.*;

@Entity
@Table(name = "loan")
@Data
public class Loan {

    @Id
    @GeneratedValue
    private Long id;

    @Column
    private int term;

    @Column
    private double amount;

    @Column
    private Date created;

    @Column
    private Date expires;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "loan_id", referencedColumnName = "id")
    private List<LoanExtension> loanExtensions = new ArrayList<>();
}