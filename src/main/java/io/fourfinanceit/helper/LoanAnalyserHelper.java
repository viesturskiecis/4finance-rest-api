package io.fourfinanceit.helper;

import io.fourfinanceit.data.dto.LoanRequestDto;
import io.fourfinanceit.rule.LoanAmountAfterMidnightRule;
import io.fourfinanceit.rule.LoanFromSingleIpAddressRule;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

@Slf4j
@Component
public class LoanAnalyserHelper {
    @Autowired
    private LoanAmountAfterMidnightRule loanAmountAfterMidnightRule;

    @Autowired
    private LoanFromSingleIpAddressRule loanFromSingleIpAddressRule;

    @Autowired
    private RequestHelper requestHelper;

    public void validateLoan(LoanRequestDto loanRequestDto) {
            loanAmountAfterMidnightRule.validateLoanWithMaxAmountAfterMidnight(loanRequestDto);
            loanFromSingleIpAddressRule.validateTransactionCountLessThanAllowedCount(requestHelper.getIpAddress(), loanRequestDto.getCreated());
    }
}
