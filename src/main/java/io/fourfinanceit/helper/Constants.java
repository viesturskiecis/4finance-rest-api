package io.fourfinanceit.helper;

public class Constants {
    public static final long MAX_AMOUNT = 500;
    public static final long MIN_AMOUNT = 50;

    public static final int MAX_TERM = 30;
    public static final int MIN_TERM = 7;

    public static final double INTEREST_RATE_PER_WEEK = 0.015;

    public static final int ALLOWED_TRANSACTION_PER_DAY = 3;
    public static final int HOURS_AFTER_MIDNIGHT = 4;
}
