package io.fourfinanceit.helper;

import org.joda.time.DateTimeUtils;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import static java.util.Calendar.HOUR_OF_DAY;

public class DateUtil {
    public static int getWeeksBetween (Date startDate, Date endDate) {
        if (endDate.before(startDate)) {
            return -getWeeksBetween(endDate, startDate);
        }
        startDate = resetTime(startDate);
        endDate = resetTime(endDate);

        Calendar cal = new GregorianCalendar();
        cal.setTime(startDate);
        int weeks = 0;
        while (cal.getTime().before(endDate)) {
            cal.add(Calendar.WEEK_OF_YEAR, 1);
            weeks++;
        }
        return weeks == 0 ? 1 : weeks;
    }

    public static Date resetTime (Date d) {
        Calendar cal = new GregorianCalendar();
        cal.setTime(d);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    public static Date addDays(Date date, int days)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, days);
        return cal.getTime();
    }

    public static Date createTestDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 1, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }
}
