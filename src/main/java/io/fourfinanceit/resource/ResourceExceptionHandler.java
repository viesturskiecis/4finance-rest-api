package io.fourfinanceit.resource;

import io.fourfinanceit.data.ResponseMessage;
import io.fourfinanceit.rule.exceptions.LoanAmountAfterMidnightRuleException;
import io.fourfinanceit.rule.exceptions.LoanFromSingleIpAddressRuleException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import static io.fourfinanceit.helper.Constants.*;

@Slf4j
@ControllerAdvice
public class ResourceExceptionHandler {
    public static final String LOAN_WITH_MAX_AMOUNT_AFTER_MIDNIGHT_VIOLATION = "LOAN_WITH_MAX_AMOUNT_AFTER_MIDNIGHT_VIOLATION";
    public static final String LOAN_FROM_SINGLE_IP_ADDRESS_VIOLATION = "LOAN_FROM_SINGLE_IP_ADDRESS_VIOLATION";
    public static final String INPUT_DATA_INVALID = "INPUT_DATA_INVALID";

    @ResponseBody
    @ResponseStatus(HttpStatus.EXPECTATION_FAILED)
    @ExceptionHandler(LoanAmountAfterMidnightRuleException.class)
    public ResponseMessage loanAmountAfterMidnightRuleException(LoanAmountAfterMidnightRuleException ex) {
        return new ResponseMessage(LOAN_WITH_MAX_AMOUNT_AFTER_MIDNIGHT_VIOLATION, ex.getMessage());
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.EXPECTATION_FAILED)
    @ExceptionHandler(LoanFromSingleIpAddressRuleException.class)
    public ResponseMessage loanFromSingleIpAddressRuleException(LoanFromSingleIpAddressRuleException ex) {
        return new ResponseMessage(LOAN_FROM_SINGLE_IP_ADDRESS_VIOLATION, ex.getMessage());
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.EXPECTATION_FAILED)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseMessage inputDataInvalid(MethodArgumentNotValidException ex) {
        return new ResponseMessage(INPUT_DATA_INVALID, "Amount {" + MIN_AMOUNT + ", " + MAX_AMOUNT + "} or term {" + MIN_TERM + ", " + MAX_TERM + "} is not in range!");
    }
}
