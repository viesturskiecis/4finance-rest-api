package io.fourfinanceit.resource;

import io.fourfinanceit.data.Loan;
import io.fourfinanceit.data.LoanExtension;
import io.fourfinanceit.data.ResponseMessage;
import io.fourfinanceit.data.dto.LoanExtensionRequestDto;
import io.fourfinanceit.data.dto.LoanRequestDto;
import io.fourfinanceit.data.dto.LoanResultDto;
import io.fourfinanceit.helper.LoanAnalyserHelper;
import io.fourfinanceit.helper.RequestHelper;
import io.fourfinanceit.service.LoanService;
import io.fourfinanceit.service.TransactionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Slf4j
@RestController
@RequestMapping("loan")
public class LoanResource {

    @Autowired
    private LoanService loanService;

    @Autowired
    private TransactionService transactionService;

    @Autowired
    private LoanAnalyserHelper loanAnalyserHelper;

    @Autowired
    private RequestHelper requestHelper;

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<LoanResultDto>> getLoans() {
        log.debug("Request to get loans");
        return new ResponseEntity<>(loanService.getLoans(), HttpStatus.OK);
    }

    @RequestMapping(value = "/addLoan", method = RequestMethod.POST)
    public ResponseEntity<ResponseMessage> addLoan(@Valid @RequestBody LoanRequestDto loanRequestDto) {
        log.debug("Request to save loan");
        loanAnalyserHelper.validateLoan(loanRequestDto);
        Loan newLoan = loanService.addLoan(loanRequestDto);
        transactionService.saveTransaction(requestHelper.getIpAddress());
        ResponseMessage responseMessage = new ResponseMessage("CREATED", "New loan with ID = " + newLoan.getId() + " created");
        return new ResponseEntity<>(responseMessage, HttpStatus.OK);
    }

    @RequestMapping(value = "/addLoanExtension/{loanId}", method = RequestMethod.POST)
    public ResponseEntity<ResponseMessage> addLoanExtension(@PathVariable Long loanId, @RequestBody LoanExtensionRequestDto loanExtensionRequestDto) {
        log.debug("Request to save loanExtenstion");
        LoanExtension newLoanExtension = loanService.addLoanExtension(loanId, loanExtensionRequestDto);
        ResponseMessage responseMessage = new ResponseMessage("CREATED", "New loan extension with ID = " + newLoanExtension.getId() + " created");
        return Optional.ofNullable(newLoanExtension).map(result -> new ResponseEntity<>(responseMessage, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
}
