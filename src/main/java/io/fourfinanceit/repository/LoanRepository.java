package io.fourfinanceit.repository;

import io.fourfinanceit.data.Loan;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LoanRepository extends JpaRepository<Loan, Long> {
}
