package io.fourfinanceit.repository;

import io.fourfinanceit.data.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.Date;

public interface TransactionRepository extends JpaRepository <Transaction, Long> {
    Long countByIpAddressAndCreated(String ipAddress, Date date);
}
