package io.fourfinanceit.repository;

import io.fourfinanceit.data.LoanExtension;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LoanExtensionRepository  extends JpaRepository<LoanExtension, Long> {
    LoanExtension findByLoanId(Long loanId);
}
