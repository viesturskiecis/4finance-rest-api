package io.fourfinanceit.rule;

import io.fourfinanceit.rule.exceptions.LoanFromSingleIpAddressRuleException;
import io.fourfinanceit.service.TransactionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

import static io.fourfinanceit.helper.Constants.ALLOWED_TRANSACTION_PER_DAY;

@Slf4j
@Component
public class LoanFromSingleIpAddressRule {

    @Autowired
    TransactionService transactionService;

    public void validateTransactionCountLessThanAllowedCount(String ipAddress, Date date) {
        Long transactionCount = transactionService.getTransactionCountForDate(ipAddress, date);
        log.debug("Transaction count from single IP per day exceeds norm : " + transactionCount);
        if (transactionCount >= ALLOWED_TRANSACTION_PER_DAY) {
            throw new LoanFromSingleIpAddressRuleException("Transaction count from single IP per day exceeds norm");
        }
    }

}
