package io.fourfinanceit.rule;

import io.fourfinanceit.data.dto.LoanRequestDto;
import io.fourfinanceit.rule.exceptions.LoanAmountAfterMidnightRuleException;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.Date;

import static io.fourfinanceit.helper.Constants.HOURS_AFTER_MIDNIGHT;
import static io.fourfinanceit.helper.Constants.MAX_AMOUNT;

@Slf4j
@Component
public class LoanAmountAfterMidnightRule {

    public void validateLoanWithMaxAmountAfterMidnight(LoanRequestDto loanRequestDto) {
        if (isLoanMaxAmount(loanRequestDto) && isLoanAfterMidnight(loanRequestDto)) {
            log.debug("Loan with max amount taken at " + loanRequestDto.getCreated());
            throw new LoanAmountAfterMidnightRuleException("Loan with max amount taken at " + DateTime.now().toDate());
        }
    }

    private boolean isLoanMaxAmount(LoanRequestDto loanRequestDto) {
        return loanRequestDto.getAmount() == MAX_AMOUNT;
    }

    private boolean isLoanAfterMidnight(LoanRequestDto loanRequestDto) {
        Date loanCreated = loanRequestDto.getCreated();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(loanCreated);
        int hours = calendar.get(Calendar.HOUR_OF_DAY);
        return hours < HOURS_AFTER_MIDNIGHT;
    }
}
