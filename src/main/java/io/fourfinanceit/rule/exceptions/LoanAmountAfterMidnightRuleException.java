package io.fourfinanceit.rule.exceptions;

public class LoanAmountAfterMidnightRuleException extends RuntimeException {
    public LoanAmountAfterMidnightRuleException(String message) {
        super(message);
    }
}
