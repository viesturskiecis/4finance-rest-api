package io.fourfinanceit.rule.exceptions;

public class LoanFromSingleIpAddressRuleException extends RuntimeException {
    public LoanFromSingleIpAddressRuleException(String message) {
        super(message);
    }
}
