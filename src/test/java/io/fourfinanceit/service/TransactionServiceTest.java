package io.fourfinanceit.service;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import io.fourfinanceit.data.Transaction;
import io.fourfinanceit.helper.DateUtil;
import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
@DatabaseSetup("/transaction-dataset.xml")
@DatabaseTearDown("/transaction-dataset.xml")
public class TransactionServiceTest {
    private static final String IP_ADDRESS = "localhost";

    @Autowired
    private TransactionService transactionService;

    @Test
    public void testSaveTransaction() {
        Transaction transaction = transactionService.saveTransaction(IP_ADDRESS);
        assertEquals(transaction.getIpAddress(), IP_ADDRESS);
    }

    @Test
    public void testGetTransactionCountForDate() {
        Long transactionCount = transactionService.getTransactionCountForDate(IP_ADDRESS, DateUtil.createTestDate());
        assertEquals(transactionCount, Long.valueOf(3));
    }
}
