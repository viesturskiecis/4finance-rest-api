package io.fourfinanceit.service;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import io.fourfinanceit.data.Loan;
import io.fourfinanceit.data.LoanExtension;
import io.fourfinanceit.data.dto.LoanExtensionRequestDto;
import io.fourfinanceit.data.dto.LoanRequestDto;
import io.fourfinanceit.data.dto.LoanResultDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)

@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
public class LoanServiceTest {

    @Autowired
    private LoanService loanService;

    @Test
    public void testAddLoan() {
        Loan loan = loanService.addLoan(new LoanRequestDto(100, 10));
        assertEquals(loan.getAmount(), 100, 0);
        assertEquals(loan.getTerm(), 10);
    }

    @Test
    public void testAddLoanExtension() {
        Loan loan = loanService.addLoan(new LoanRequestDto(100, 10));
        LoanExtension loanExtension = loanService.addLoanExtension(loan.getId(), new LoanExtensionRequestDto(10));
        assertEquals(loanExtension.getExtension(), 10);
    }

    @Test
    @DatabaseSetup("/loan-dataset.xml")
    @DatabaseTearDown("/loan-dataset.xml")
    public void testGetLoans() {
        LoanResultDto loanResultDto = loanService.getLoans().get(0);
        assertEquals(loanResultDto.getAmount(), 200, 0);
        assertEquals(loanResultDto.getTerm(), 10);
        assertEquals(loanResultDto.getExtensionCount(), 1);
        assertEquals(loanResultDto.getExtensionTerm(), 10);
        assertEquals(loanResultDto.getAmountToPay(), 206, 0);
    }
}
