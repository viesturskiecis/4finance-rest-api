package io.fourfinanceit.rule;

import io.fourfinanceit.data.dto.LoanRequestDto;
import io.fourfinanceit.helper.DateUtil;
import io.fourfinanceit.rule.exceptions.LoanAmountAfterMidnightRuleException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import java.util.Date;

import static io.fourfinanceit.helper.Constants.MAX_AMOUNT;
import static io.fourfinanceit.helper.Constants.MIN_TERM;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        TransactionalTestExecutionListener.class})
public class LoanAmountAfterMidnightRuleTest {
    @Autowired
    LoanAmountAfterMidnightRule loanAmountAfterMidnightRule;

    @Test(expected = LoanAmountAfterMidnightRuleException.class)
    public void testLoanTakenShortAfterMignight(){
        Date loanCreated = DateUtil.createTestDate();
        LoanRequestDto loanRequestDto = new LoanRequestDto(MAX_AMOUNT, MIN_TERM, loanCreated);
        loanAmountAfterMidnightRule.validateLoanWithMaxAmountAfterMidnight(loanRequestDto);
    }
}
