package io.fourfinanceit.rule;

import io.fourfinanceit.helper.DateUtil;
import io.fourfinanceit.repository.TransactionRepository;
import io.fourfinanceit.rule.exceptions.LoanFromSingleIpAddressRuleException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
@DatabaseSetup("/transaction-dataset.xml")
@DatabaseTearDown("/transaction-dataset.xml")
public class LoanFromSingleIpAddressRuleTest {
    private static final String IP_ADDRESS = "localhost";

    @Autowired
    LoanFromSingleIpAddressRule loanFromSingleIpAddressRule;

    @Autowired
    private TransactionRepository transactionRepository;

    @Test(expected = LoanFromSingleIpAddressRuleException.class)
    public void testLoanRequestMultipleTimesFromSingleIp(){
        loanFromSingleIpAddressRule.validateTransactionCountLessThanAllowedCount(IP_ADDRESS, DateUtil.createTestDate());
    }
}
