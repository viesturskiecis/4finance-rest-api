package io.fourfinanceit.helper;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

import java.util.Date;

import static org.junit.Assert.*;

@RunWith(BlockJUnit4ClassRunner.class)
public class DateUtilTest {
    @Test
    public void testAddDays() {
        assertEquals(DateUtil.addDays(new Date(2017, 9, 12), 10), new Date(2017, 9, 22));
    }

    @Test
    public void testGetWeeksBetween() {
        assertEquals(DateUtil.getWeeksBetween(new Date(2017, 9, 12), new Date(2017, 9, 28)), 3);
    }
}
