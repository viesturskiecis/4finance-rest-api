package io.fourfinanceit.resource;

import io.fourfinanceit.data.ResponseMessage;
import io.fourfinanceit.data.dto.LoanExtensionRequestDto;
import io.fourfinanceit.data.dto.LoanRequestDto;
import io.fourfinanceit.data.dto.LoanResultDto;
import io.fourfinanceit.helper.DateUtil;
import org.joda.time.DateTimeUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

import static org.junit.Assert.assertEquals;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class LoanResourceTest {
    private static final String LOAN_PATH_PREFIX = "/loan/";
    private static final String LOAN_PATH = LOAN_PATH_PREFIX + "addLoan/";
    private static final String LOAN_EXTENSION_PATH = LOAN_PATH_PREFIX + "addLoanExtension/";

    @Before
    public void before() throws Exception {
        Date fixedDateTime = DateUtil.createTestDate();
        DateTimeUtils.setCurrentMillisFixed(fixedDateTime.getTime());
    }

    @After
    public void after() throws Exception {
        DateTimeUtils.setCurrentMillisSystem();
    }

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void testAddLoanAndLoanExtension() {
        ResponseEntity<ResponseMessage> loanResponse = createLoan(new LoanRequestDto(200, 10));
        assertEquals(HttpStatus.OK, loanResponse.getStatusCode());
        ResponseEntity<ResponseMessage> loanExtensionResponse = createLoanExtension(new LoanExtensionRequestDto(10), 1L);
        assertEquals(HttpStatus.OK, loanExtensionResponse.getStatusCode());
        LoanResultDto[] loans = getLoans().getBody();
        LoanResultDto loanResultDto = loans[0];
        assertEquals(loanResultDto.getAmountToPay(), 206, 0);
        assertEquals(loanResultDto.getExtensionCount(), 1);
        assertEquals(loanResultDto.getExtensionTerm(), 10);
    }

    @Test
    public void testAddLoanInvalidInput() {
        LoanRequestDto loanRequestDto = new LoanRequestDto(5, 1);
        ResponseEntity<ResponseMessage> loanResponse = createLoan(loanRequestDto);
        assertEquals(HttpStatus.EXPECTATION_FAILED, loanResponse.getStatusCode());
        assertEquals(loanResponse.getBody().getCode(), ResourceExceptionHandler.INPUT_DATA_INVALID);
    }

    @Test
    public void testAddLoanAfterMidnightWithMaxAmount() {
        LoanRequestDto loanRequestDto = new LoanRequestDto(500, 10);
        ResponseEntity<ResponseMessage> loanResponse = createLoan(loanRequestDto);
        assertEquals(HttpStatus.EXPECTATION_FAILED, loanResponse.getStatusCode());
        assertEquals(loanResponse.getBody().getCode(), ResourceExceptionHandler.LOAN_WITH_MAX_AMOUNT_AFTER_MIDNIGHT_VIOLATION);
    }

    @Test
    public void testAddLoanFromSingleIpAddressMultipleTimes() {
        LoanRequestDto loanRequestDto = new LoanRequestDto(100, 10);
        ResponseEntity<ResponseMessage> loanResponse = createLoan(loanRequestDto);
        assertEquals(HttpStatus.OK, loanResponse.getStatusCode());
        loanResponse = createLoan(loanRequestDto);
        assertEquals(HttpStatus.OK, loanResponse.getStatusCode());
        loanResponse = createLoan(loanRequestDto);
        assertEquals(HttpStatus.OK, loanResponse.getStatusCode());
         loanResponse = createLoan(loanRequestDto);
        assertEquals(loanResponse.getBody().getCode(), ResourceExceptionHandler.LOAN_FROM_SINGLE_IP_ADDRESS_VIOLATION);
    }

    public ResponseEntity<LoanResultDto[]> getLoans() {
        return this.restTemplate.getForEntity(LOAN_PATH_PREFIX, LoanResultDto[].class);
    }

    public ResponseEntity<ResponseMessage> createLoan(LoanRequestDto loanRequestDto) {
        return this.restTemplate.postForEntity(LOAN_PATH, loanRequestDto, ResponseMessage.class);
    }

    public ResponseEntity<ResponseMessage> createLoanExtension(LoanExtensionRequestDto loanExtensionRequestDto, Long loanId) {
        return this.restTemplate.postForEntity(LOAN_EXTENSION_PATH + loanId, loanExtensionRequestDto, ResponseMessage.class);
    }

    public LoanResultDto intitializeLoanResult() {
        LoanResultDto loanResultDto = new LoanResultDto();
        return loanResultDto;
    }
}
